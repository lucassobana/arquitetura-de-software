from uuid import uuid4

class Entity:

    def __init__[T](self,props: T, _id: str | None):
        self.props: T = props
        if _id is None:
            self._id = str(uuid4())
        else:
            self._id = _id
