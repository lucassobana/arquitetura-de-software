from domain.entities.car import Car

class CreateCarUseCase:

    def __init__(self, car_repository):
        self.repository = car_repository
        



    def execute(self, car: Car):
        self.repository.create(car)