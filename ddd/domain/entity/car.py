from core.base_entity import Entity
from dataclasses import dataclass


@dataclass()
class CarProps:
    color: str
    model: str

class Car(Entity):
    def __init__(self, props: T, _id: str | None):
        super(Car, self).__init__(props, _id)
